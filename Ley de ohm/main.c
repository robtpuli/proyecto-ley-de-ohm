//
//  main.c
//  Proyecto final
//
//  Created by ROBERTO PULIDO OROZCO and ARMANDO ZARCO HERNANDEZ on 01/12/20.
//

#include <stdio.h>
#include <stdlib.h>

int main (){
    
    int menu1;
    
    
do {
    
        
    printf("\n\n---LEY DE OHM---\n\n");  //Titulo
    
    printf("La ley de Ohm se usa para determinar la relación entre tensión, corriente y resistencia en un circuito eléctrico. \nSi se conocen dos de estos valores, los técnicos pueden reconfigurar la ley de Ohm para calcular el tercero.\nEste programa se encarga de hacer la resolucion de circuitos electricos de corriente directa.\n\n"); //Menu principal
    printf("--Menu Principal--\n\n");
    printf("1. Circuito serie\n");
    printf("2. Circuito paralelo\n");
    printf("3. Salir\n");
    printf("Elija una opcion:");
    scanf("%d", &menu1);


    int Resistencias;
    int Voltajes;
    float R, V, I;
   
        switch (menu1) {
            case 1:{ //CIRCUITO SERIE
                int menu2;
              
                do {
                    printf("\n\n--Circuito serie--\n\n"); //Menu circuito serie
                    printf("1. Calcular el Voltaje\n");
                    printf("2. Calcular la Resistencia\n");
                    printf("3. Calcular la Corriente\n");
                    printf("4. Salir\n");
                    printf("Elije una opcion: ");
                    scanf("%d", &menu2);
                    
                    
                    switch (menu2) {
                        case 1:{ // 1. Calcular Voltaje
                            int menu3;
                            do {
                                printf("\n\n¿Cuantos voltajes tiene su circuito?: ");
                                scanf("%d",&Voltajes); //# de resistencias
                                
                           
                                    switch (Voltajes) {
                                            
                                        case 1:
                                             { //Voltajes = 1
                                                printf("\n\nIntroduzca el valor de R: "); scanf("%f", &R);
                                                printf("Introduzca el valor de I: "); scanf("%f", &I);
                                                   V = R*I;
                                                printf("\nEl Valor de V = %f volts", V);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 printf("(-) ---*--- %.2fvolts(+)",V);
                                             }break;
                                            
                                        case 2:
                                             { //Voltajes = 2
                                             float r1, r2, v1, v2, r, i, v;
                                                   
                                                 printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                                 printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    v1 = r1*i;
                                                    v2 = r2*i;
                                                    r = r1+r2;
                                                    v = v1+v2;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\nEl valor de V1 = %.3f volts",v1);
                                                 printf("\nEl valor de V2 = %.3f volts",v2);
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 printf("(-) ---*---*--- %.2fvolts(+)",v);
                                             }break;
                                            
                                        case 3:
                                             { //Voltajes 3
                                             float r1, r2, r3, v1, v2, v3, r, i, v;
                                                   
                                                 printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                                 printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                                 printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    v1 = r1*i;
                                                    v2 = r2*i;
                                                    v3 = r3*i;
                                                    r = r1+r2+r3;
                                                    v = v1+v2+v3;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\nEl valor de V1 = %.3f volts",v1);
                                                 printf("\nEl valor de V2 = %.3f volts",v2);
                                                 printf("\nEl valor de V3 = %.3f volts",v3);
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Voltajes; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Voltajes) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Voltajes-1) {
                                                             printf("|\n*");}
                                                         if (ren==Voltajes-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes-1) {
                                                             printf("*");}
                                                         if (ren==Voltajes && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Voltajes && col==Voltajes) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }
                                             }break;
                                            
                                        case 4:
                                             { //Voltajes 4
                                             float r1, r2, r3, r4, i, v1, v2, v3, v4, r, v;
                                               
                                                 printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                                 printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                                 printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                                 printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    v1 = r1*i;
                                                    v2 = r2*i;
                                                    v3 = r3*i;
                                                    v4 = r4*i;
                                                    r = r1+r2+r3+r4;
                                                    v = v1+v2+v3+v4;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\nEl valor de V1 = %.3f volts",v1);
                                                 printf("\nEl valor de V2 = %.3f volts",v2);
                                                 printf("\nEl valor de V3 = %.3f volts",v3);
                                                 printf("\nEl valor de V4 = %.3f volts",v4);
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Voltajes; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Voltajes) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Voltajes-1) {
                                                             printf("|\n*");}
                                                         if (ren==Voltajes-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes-1) {
                                                             printf("*");}
                                                         if (ren==Voltajes && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Voltajes && col==Voltajes) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }

                                             }break;
                                
                                        case 5:
                                             { //Voltajes 5
                                             float r1, r2, r3, r4, r5, v1, v2, v3, v4, v5, r, i, v;
                                               
                                                 printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                                 printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                                 printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                                 printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                                 printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    v1 = r1*i;
                                                    v2 = r2*i;
                                                    v3 = r3*i;
                                                    v4 = r4*i;
                                                    v5 = r5*i;
                                                    r = r1+r2+r3+r4+r5;
                                                    v = v1+v2+v3+v4+v5;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\nEl valor de V1 = %.3f volts",v1);
                                                 printf("\nEl valor de V2 = %.3f volts",v2);
                                                 printf("\nEl valor de V3 = %.3f volts",v3);
                                                 printf("\nEl valor de V4 = %.3f volts",v4);
                                                 printf("\nEl valor de V5 = %.3f volts",v5);
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Voltajes; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Voltajes) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Voltajes-1) {
                                                             printf("|\n*");}
                                                         if (ren==Voltajes-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes-1) {
                                                             printf("*");}
                                                         if (ren==Voltajes && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Voltajes && col==Voltajes) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }

                                             }break;
                                       
                                        case 6:
                                             { //Voltajes 6
                                             float r1, r2, r3, r4, r5, r6, v1, v2, v3, v4, v5, v6, r, i, v;
                                          
                                                 printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                                 printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                                 printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                                 printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                                 printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                                 printf("Introduzca el valor de R6: "); scanf("%f", &r6);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    v1 = r1*i;
                                                    v2 = r2*i;
                                                    v3 = r3*i;
                                                    v4 = r4*i;
                                                    v5 = r5*i;
                                                    v6 = r6*i;
                                                    r = r1+r2+r3+r4+r5+r6;
                                                    v = v1+v2+v3+v4+v5+v6;
                                                 printf("\n--Valores--");
                                                printf("\nEl valor de R = %.3f ohms",r);
                                                printf("\nEl valor de V1 = %.3f volts",v1);
                                                printf("\nEl valor de V2 = %.3f volts",v2);
                                                printf("\nEl valor de V3 = %.3f volts",v3);
                                                printf("\nEl valor de V4 = %.3f volts",v4);
                                                printf("\nEl valor de V5 = %.3f volts",v5);
                                                printf("\nEl valor de V6 = %.3f volts",v6);
                                                printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Voltajes; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Voltajes) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Voltajes-1) {
                                                             printf("|\n*");}
                                                         if (ren==Voltajes-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes-1) {
                                                             printf("*");}
                                                         if (ren==Voltajes && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Voltajes) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Voltajes && col==Voltajes) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }

                                             }break;
                                            
                                        case 7:
                                        { //Resistencias 7
                                        float r1, r2, r3, r4, r5, r6, r7, v1, v2, v3, v4, v5, v6, v7, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                            printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                            printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                            printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                            printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                            printf("Introduzca el valor de R6: "); scanf("%f", &r6);
                                            printf("Introduzca el valor de R7: "); scanf("%f", &r7);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               v1 = r1*i;
                                               v2 = r2*i;
                                               v3 = r3*i;
                                               v4 = r4*i;
                                               v5 = r5*i;
                                               v6 = r6*i;
                                               v7 = r7*i;
                                               r = r1+r2+r3+r4+r5+r6+r7;
                                               v = v1+v2+v3+v4+v5+v6+v7;
                                            printf("\n--Valores--");
                                           printf("\nEl valor de R = %.3f ohms",r);
                                           printf("\nEl valor de V1 = %.3f volts",v1);
                                           printf("\nEl valor de V2 = %f.3 volts",v2);
                                           printf("\nEl valor de V3 = %.3f volts",v3);
                                           printf("\nEl valor de V4 = %.3f volts",v4);
                                           printf("\nEl valor de V5 = %.3f volts",v5);
                                           printf("\nEl valor de V6 = %.3f volts",v6);
                                           printf("\nEl valor de V7 = %.3f volts",v7);
                                           printf("\nEl valor de V = %.3f volts",v);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                
                                                for (int col=1; col<=Voltajes; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Voltajes) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Voltajes-1) {
                                                        printf("|\n*");}
                                                    if (ren==Voltajes-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes-1) {
                                                        printf("*");}
                                                    if (ren==Voltajes && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Voltajes && col==Voltajes) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }

                                        }break;
                                        
                                        case 8:
                                        { //voltajes 8
                                        float r1, r2, r3, r4, r5, r6, r7, r8, v1, v2, v3, v4, v5, v6, v7, v8, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                            printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                            printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                            printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                            printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                            printf("Introduzca el valor de R6: "); scanf("%f", &r6);
                                            printf("Introduzca el valor de R7: "); scanf("%f", &r7);
                                            printf("Introduzca el valor de R8: "); scanf("%f", &r8);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               v1 = r1*i;
                                               v2 = r2*i;
                                               v3 = r3*i;
                                               v4 = r4*i;
                                               v5 = r5*i;
                                               v6 = r6*i;
                                               v7 = r7*i;
                                               v8 = r8*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8;
                                            printf("\n--Valores--");
                                           printf("\nEl valor de R = %.3f ohms",r);
                                           printf("\nEl valor de V1 = %.3f volts",v1);
                                           printf("\nEl valor de V2 = %.3f volts",v2);
                                           printf("\nEl valor de V3 = %.3f volts",v3);
                                           printf("\nEl valor de V4 = %.3f volts",v4);
                                           printf("\nEl valor de V5 = %.3f volts",v5);
                                           printf("\nEl valor de V6 = %.3f volts",v6);
                                           printf("\nEl valor de V7 = %.3f volts",v7);
                                           printf("\nEl valor de V8 = %.3f volts",v8);
                                           printf("\nEl valor de V = %.3f volts",v);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                
                                                for (int col=1; col<=Voltajes; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Voltajes) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Voltajes-1) {
                                                        printf("|\n*");}
                                                    if (ren==Voltajes-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes-1) {
                                                        printf("*");}
                                                    if (ren==Voltajes && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Voltajes && col==Voltajes) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }

                                        }break;
                                        
                                        case 9:
                                        { //voltajes 9
                                        float r1, r2, r3, r4, r5, r6, r7, r8, r9, v1, v2, v3, v4, v5, v6, v7, v8, v9, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                            printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                            printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                            printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                            printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                            printf("Introduzca el valor de R6: "); scanf("%f", &r6);
                                            printf("Introduzca el valor de R7: "); scanf("%f", &r7);
                                            printf("Introduzca el valor de R8: "); scanf("%f", &r8);
                                            printf("Introduzca el valor de R9: "); scanf("%f", &r9);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               v1 = r1*i;
                                               v2 = r2*i;
                                               v3 = r3*i;
                                               v4 = r4*i;
                                               v5 = r5*i;
                                               v6 = r6*i;
                                               v7 = r7*i;
                                               v8 = r8*i;
                                               v9 = r9*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8+r9;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8+v9;
                                            printf("\n--Valores--");
                                           printf("\nEl valor de R = %.3f ohms",r);
                                           printf("\nEl valor de V1 = %.3f volts",v1);
                                           printf("\nEl valor de V2 = %.3f volts",v2);
                                           printf("\nEl valor de V3 = %.3f volts",v3);
                                           printf("\nEl valor de V4 = %.3f volts",v4);
                                           printf("\nEl valor de V5 = %.3f volts",v5);
                                           printf("\nEl valor de V6 = %.3f volts",v6);
                                           printf("\nEl valor de V7 = %.3f volts",v7);
                                           printf("\nEl valor de V8 = %.3f volts",v8);
                                           printf("\nEl valor de V9 = %.3f volts",v9);
                                           printf("\nEl valor de V = %.3f volts",v);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                
                                                for (int col=1; col<=Voltajes; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Voltajes) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Voltajes-1) {
                                                        printf("|\n*");}
                                                    if (ren==Voltajes-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes-1) {
                                                        printf("*");}
                                                    if (ren==Voltajes && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Voltajes) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Voltajes && col==Voltajes) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }

                                        }break;
                                        
                                        case 10:
                                            { //voltajes 10
                                            float r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de R1: "); scanf("%f", &r1);
                                            printf("Introduzca el valor de R2: "); scanf("%f", &r2);
                                            printf("Introduzca el valor de R3: "); scanf("%f", &r3);
                                            printf("Introduzca el valor de R4: "); scanf("%f", &r4);
                                            printf("Introduzca el valor de R5: "); scanf("%f", &r5);
                                            printf("Introduzca el valor de R6: "); scanf("%f", &r6);
                                            printf("Introduzca el valor de R7: "); scanf("%f", &r7);
                                            printf("Introduzca el valor de R8: "); scanf("%f", &r8);
                                            printf("Introduzca el valor de R9: "); scanf("%f", &r9);
                                            printf("Introduzca el valor de R10: "); scanf("%f", &r10);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               v1 = r1*i;
                                               v2 = r2*i;
                                               v3 = r3*i;
                                               v4 = r4*i;
                                               v5 = r5*i;
                                               v6 = r6*i;
                                               v7 = r7*i;
                                               v8 = r8*i;
                                               v9 = r9*i;
                                               v10 = r10*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8+r9+r10;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8+v9+v10;
                                                printf("\n--Valores--");
                                           printf("\nEl valor de R = %.3f ohms",r);
                                           printf("\nEl valor de V1 = %.3f volts",v1);
                                           printf("\nEl valor de V2 = %.3f volts",v2);
                                           printf("\nEl valor de V3 = %.3f volts",v3);
                                           printf("\nEl valor de V4 = %.3f volts",v4);
                                           printf("\nEl valor de V5 = %.3f volts",v5);
                                           printf("\nEl valor de V6 = %.3f volts",v6);
                                           printf("\nEl valor de V7 = %.3f volts",v7);
                                           printf("\nEl valor de V8 = %.3f volts",v8);
                                           printf("\nEl valor de V9 = %.3f volts",v9);
                                           printf("\nEl valor de V10 = %.3f volts",v10);
                                           printf("\nEl valor de V = %.3f volts",v);
                                                printf("\n\n__Simbologia__");
                                                printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                for (int ren=1; ren<=Voltajes; ren++) {//renglones
                                                    
                                                    for (int col=1; col<=Voltajes; col++) {//columnas
                                                       
                                                        if (ren == 1||ren==Voltajes) {
                                                            printf("--");}
                                                        if (col==1 && ren>1 && ren<Voltajes-1) {
                                                            printf("|\n*");}
                                                        if (ren==Voltajes-1 && col==1) {
                                                            printf("|");}
                                                        if (ren==1 && col==1) {
                                                            printf("*");}
                                                        if (ren==1 && col==Voltajes-1) {
                                                            printf("*");}
                                                        if (ren==Voltajes && col==2) {
                                                            printf("*");}
                                                        if (ren==1 && col==Voltajes) {
                                                            printf(" (+)%.2fvolts",v);}
                                                        if (ren==Voltajes && col==Voltajes) {
                                                            printf(" (-)");}
                                                        
                                                    }
                                                    printf("\n");
                                                }

                                        }break;
                                        
                                        default:
                                            if (Voltajes>10) {
                                                printf("\n**El maximo de resistencias es 10**");
                                            
                                            
                                            }break;
                                            
                                    }
                                
                                printf("\n\n1. Repetir\n");
                                printf("2. Regresar\n");
                                printf("Elija una opcion:");
                                scanf("%d",&menu3);
                                
                            } while (menu3!=2);
                          } break;
                            
                        case 2:{ // 2. Calcular Resistencia
                            int menu3;
                            do{
                                printf("\n\n¿Cuantas resistencias tiene su circuito?: ");
                                scanf("%d",&Resistencias); //# de resistencias
                                
                           
                                    switch (Resistencias) {
                                            
                                        case 1:
                                             { //Resistencias = 1
                                                printf("\n\nIntroduzca el valor de V: "); scanf("%f", &V);
                                                printf("Introduzca el valor de I: "); scanf("%f", &I);
                                                   R = V/I;
                                                printf("\nEl Valor de R = %.3f ohms", R);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 printf("(-) ---*--- %.2fvolts(+)",V);
                                             }
                                             break;
                                            
                                        case 2:
                                             { //Resistencias = 2
                                             float v1, v2, r1, r2, r, i, v;
                                                   
                                                 printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                                 printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    r1 = v1/i;
                                                    r2 = v2/i;
                                                    r = r1+r2;
                                                    v = v1+v2;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\nEl valor de R1 = %.3f ohms",r1);
                                                 printf("\nEl valor de R2 = %.3f ohms",r2);
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 printf("(-) ---*---*--- %.2fvolts(+)",v);
                                             }
                                             break;
                                            
                                        case 3:
                                             { //Resistencias = 3
                                             float r1, r2, r3, v1, v2, v3, r, i, v;
                                                   
                                                 printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                                 printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                                 printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    r1 = v1/i;
                                                    r2 = v2/i;
                                                    r3 = v3/i;
                                                    r = r1+r2+r3;
                                                    v = v1+v2+v3;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\nEl valor de R1 = %.3f ohms",r1);
                                                 printf("\nEl valor de R2 = %.3f ohms",r2);
                                                 printf("\nEl valor de R3 = %.3f ohms",r3);
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Resistencias; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Resistencias) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Resistencias-1) {
                                                             printf("|\n*");}
                                                         if (ren==Resistencias-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias-1) {
                                                             printf("*");}
                                                         if (ren==Resistencias && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Resistencias && col==Resistencias) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }

                                             }
                                             break;
                                            
                                        case 4:
                                             { //Resistencias 4
                                             float r1, r2, r3, r4, i, v1, v2, v3, v4, r, v;
                                               
                                                 printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                                 printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                                 printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                                 printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    r1 = v1/i;
                                                    r2 = v2/i;
                                                    r3 = v3/i;
                                                    r4 = v4/i;
                                                    r = r1+r2+r3+r4;
                                                    v = v1+v2+v3+v4;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\nEl valor de R1 = %.3f ohms",r1);
                                                 printf("\nEl valor de R2 = %.3f ohms",r2);
                                                 printf("\nEl valor de R3 = %.3f ohms",r3);
                                                 printf("\nEl valor de R4 = %.3f ohms",r4);
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Resistencias; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Resistencias) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Resistencias-1) {
                                                             printf("|\n*");}
                                                         if (ren==Resistencias-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias-1) {
                                                             printf("*");}
                                                         if (ren==Resistencias && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Resistencias && col==Resistencias) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }
                                             }
                                             break;
                                
                                        case 5:
                                             { //Resistencias 5
                                             float r1, r2, r3, r4, r5, v1, v2, v3, v4, v5, r, i, v;
                                               
                                                 printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                                 printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                                 printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                                 printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                                 printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    r1 = v1/i;
                                                    r2 = v2/i;
                                                    r3 = v3/i;
                                                    r4 = v4/i;
                                                    r5 = v5/i;
                                                    r = r1+r2+r3+r4+r5;
                                                    v = v1+v2+v3+v4+v5;
                                                 printf("\n--Valores--");
                                                 printf("\nEl valor de V = %.3f volts",v);
                                                 printf("\nEl valor de R1 = %.3f ohms",r1);
                                                 printf("\nEl valor de R2 = %.3f ohms",r2);
                                                 printf("\nEl valor de R3 = %.3f ohms",r3);
                                                 printf("\nEl valor de R4 = %.3f ohms",r4);
                                                 printf("\nEl valor de R5 = %.3f ohms",r5);
                                                 printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Resistencias; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Resistencias) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Resistencias-1) {
                                                             printf("|\n*");}
                                                         if (ren==Resistencias-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias-1) {
                                                             printf("*");}
                                                         if (ren==Resistencias && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Resistencias && col==Resistencias) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }
                                             }
                                             break;
                                       
                                        case 6:
                                             { //Resistencias 6
                                             float r1, r2, r3, r4, r5, r6, v1, v2, v3, v4, v5, v6, r, i, v;
                                          
                                                 printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                                 printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                                 printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                                 printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                                 printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                                 printf("Introduzca el valor de V6: "); scanf("%f", &v6);
                                                 printf("Introduzca el valor de I: "); scanf("%f", &i);
                                                    r1 = v1*i;
                                                    r2 = v2*i;
                                                    r3 = v3*i;
                                                    r4 = v4*i;
                                                    r5 = v5*i;
                                                    r6 = v6*i;
                                                    r = r1+r2+r3+r4+r5+r6;
                                                    v = v1+v2+v3+v4+v5+v6;
                                                 printf("\n--Valores--");
                                                printf("\nEl valor de V = %.3f volts",v);
                                                printf("\nEl valor de R1 = %.3f ohms",r1);
                                                printf("\nEl valor de R2 = %.3f ohms",r2);
                                                printf("\nEl valor de R3 = %.3f ohms",r3);
                                                printf("\nEl valor de R4 = %.3f ohms",r4);
                                                printf("\nEl valor de R5 = %.3f ohms",r5);
                                                printf("\nEl valor de R6 = %.3f ohms",r6);
                                                printf("\nEl valor de R = %.3f ohms",r);
                                                 printf("\n\n__Simbologia__");
                                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                 for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                     
                                                     for (int col=1; col<=Resistencias; col++) {//columnas
                                                        
                                                         if (ren == 1||ren==Resistencias) {
                                                             printf("--");}
                                                         if (col==1 && ren>1 && ren<Resistencias-1) {
                                                             printf("|\n*");}
                                                         if (ren==Resistencias-1 && col==1) {
                                                             printf("|");}
                                                         if (ren==1 && col==1) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias-1) {
                                                             printf("*");}
                                                         if (ren==Resistencias && col==2) {
                                                             printf("*");}
                                                         if (ren==1 && col==Resistencias) {
                                                             printf(" (+)%.2fvolts",v);}
                                                         if (ren==Resistencias && col==Resistencias) {
                                                             printf(" (-)");}
                                                         
                                                     }
                                                     printf("\n");
                                                 }
                                             }
                                             break;
                                            
                                        case 7:
                                        { //Resistencias 7
                                        float r1, r2, r3, r4, r5, r6, r7, v1, v2, v3, v4, v5, v6, v7, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                            printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                            printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                            printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                            printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                            printf("Introduzca el valor de V6: "); scanf("%f", &v6);
                                            printf("Introduzca el valor de V7: "); scanf("%f", &v7);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               r1 = v1*i;
                                               r2 = v2*i;
                                               r3 = v3*i;
                                               r4 = v4*i;
                                               r5 = v5*i;
                                               r6 = v6*i;
                                               r7 = v7*i;
                                               r = r1+r2+r3+r4+r5+r6+r7;
                                               v = v1+v2+v3+v4+v5+v6+v7;
                                           printf("\nEl valor de V = %.3f volts",v);
                                           printf("\nEl valor de R1 = %.3f ohms",r1);
                                           printf("\nEl valor de R2 = %.3f ohms",r2);
                                           printf("\nEl valor de R3 = %.3f ohms",r3);
                                           printf("\nEl valor de R4 = %.3f ohms",r4);
                                           printf("\nEl valor de R5 = %.3f ohms",r5);
                                           printf("\nEl valor de R6 = %.3f ohms",r6);
                                           printf("\nEl valor de R7 = %.3f ohms",r7);
                                           printf("\nEl valor de R = %.3f ohms",r);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                
                                                for (int col=1; col<=Resistencias; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Resistencias) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Resistencias-1) {
                                                        printf("|\n*");}
                                                    if (ren==Resistencias-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias-1) {
                                                        printf("*");}
                                                    if (ren==Resistencias && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Resistencias && col==Resistencias) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }
                                        }
                                            break;
                                        
                                        case 8:
                                        { //Resistencias 8
                                        float r1, r2, r3, r4, r5, r6, r7, r8, v1, v2, v3, v4, v5, v6, v7, v8, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                            printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                            printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                            printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                            printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                            printf("Introduzca el valor de V6: "); scanf("%f", &v6);
                                            printf("Introduzca el valor de V7: "); scanf("%f", &v7);
                                            printf("Introduzca el valor de V8: "); scanf("%f", &v8);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               r1 = v1*i;
                                               r2 = v2*i;
                                               r3 = v3*i;
                                               r4 = v4*i;
                                               r5 = v5*i;
                                               r6 = v6*i;
                                               r7 = v7*i;
                                               r8 = v8*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8;
                                            printf("\n--Valores--");
                                           printf("\nEl valor de V = %.3f volts",v);
                                           printf("\nEl valor de R1 = %.3f ohms",r1);
                                           printf("\nEl valor de R2 = %.3f ohms",r2);
                                           printf("\nEl valor de R3 = %.3f ohms",r3);
                                           printf("\nEl valor de R4 = %.3f ohms",r4);
                                           printf("\nEl valor de R5 = %.3f ohms",r5);
                                           printf("\nEl valor de R6 = %.3f ohms",r6);
                                           printf("\nEl valor de R7 = %.3f ohms",r7);
                                           printf("\nEl valor de R8 = %.3f ohms",r8);
                                           printf("\nEl valor de R = %.3f ohms",r);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                
                                                for (int col=1; col<=Resistencias; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Resistencias) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Resistencias-1) {
                                                        printf("|\n*");}
                                                    if (ren==Resistencias-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias-1) {
                                                        printf("*");}
                                                    if (ren==Resistencias && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Resistencias && col==Resistencias) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }
                                        }
                                            break;
                                        
                                        case 9:
                                        { //Resistencias 9
                                        float r1, r2, r3, r4, r5, r6, r7, r8, r9, v1, v2, v3, v4, v5, v6, v7, v8, v9, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                            printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                            printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                            printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                            printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                            printf("Introduzca el valor de V6: "); scanf("%f", &v6);
                                            printf("Introduzca el valor de V7: "); scanf("%f", &v7);
                                            printf("Introduzca el valor de V8: "); scanf("%f", &v8);
                                            printf("Introduzca el valor de V9: "); scanf("%f", &v9);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               r1 = v1*i;
                                               r2 = v2*i;
                                               r3 = v3*i;
                                               r4 = v4*i;
                                               r5 = v5*i;
                                               r6 = v6*i;
                                               r7 = v7*i;
                                               r8 = v8*i;
                                               r9 = v9*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8+r9;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8+v9;
                                            printf("\n--Valores--");
                                           printf("\nEl valor de V = %.3f volts",v);
                                           printf("\nEl valor de R1 = %.3f ohms",r1);
                                           printf("\nEl valor de R2 = %.3f ohms",r2);
                                           printf("\nEl valor de R3 = %.3f ohms",r3);
                                           printf("\nEl valor de R4 = %.3f ohms",r4);
                                           printf("\nEl valor de R5 = %.3f ohms",r5);
                                           printf("\nEl valor de R6 = %.3f ohms",r6);
                                           printf("\nEl valor de R7 = %.3f ohms",r7);
                                           printf("\nEl valor de R8 = %.3f ohms",r8);
                                           printf("\nEl valor de R9 = %.3f ohms",r9);
                                           printf("\nEl valor de R = %.3f ohms",r);
                                            printf("\n\n__Simbologia__");
                                            printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                            for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                
                                                for (int col=1; col<=Resistencias; col++) {//columnas
                                                   
                                                    if (ren == 1||ren==Resistencias) {
                                                        printf("--");}
                                                    if (col==1 && ren>1 && ren<Resistencias-1) {
                                                        printf("|\n*");}
                                                    if (ren==Resistencias-1 && col==1) {
                                                        printf("|");}
                                                    if (ren==1 && col==1) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias-1) {
                                                        printf("*");}
                                                    if (ren==Resistencias && col==2) {
                                                        printf("*");}
                                                    if (ren==1 && col==Resistencias) {
                                                        printf(" (+)%.2fvolts",v);}
                                                    if (ren==Resistencias && col==Resistencias) {
                                                        printf(" (-)");}
                                                    
                                                }
                                                printf("\n");
                                            }
                                        }
                                            break;
                                        
                                        case 10:
                                            { //Resistencias 10
                                            float r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, r, i, v;
                                     
                                            printf("\nIntroduzca el valor de V1: "); scanf("%f", &v1);
                                            printf("Introduzca el valor de V2: "); scanf("%f", &v2);
                                            printf("Introduzca el valor de V3: "); scanf("%f", &v3);
                                            printf("Introduzca el valor de V4: "); scanf("%f", &v4);
                                            printf("Introduzca el valor de V5: "); scanf("%f", &v5);
                                            printf("Introduzca el valor de V6: "); scanf("%f", &v6);
                                            printf("Introduzca el valor de V7: "); scanf("%f", &v7);
                                            printf("Introduzca el valor de V8: "); scanf("%f", &v8);
                                            printf("Introduzca el valor de V9: "); scanf("%f", &v9);
                                            printf("Introduzca el valor de V10: "); scanf("%f", &v10);
                                            printf("Introduzca el valor de I: "); scanf("%f", &i);
                                               r1 = v1*i;
                                               r2 = v2*i;
                                               r3 = v3*i;
                                               r4 = v4*i;
                                               r5 = v5*i;
                                               r6 = v6*i;
                                               r7 = v7*i;
                                               r8 = v8*i;
                                               r9 = v9*i;
                                               r10 = v10*i;
                                               r = r1+r2+r3+r4+r5+r6+r7+r8+r9+r10;
                                               v = v1+v2+v3+v4+v5+v6+v7+v8+v9+v10;
                                                printf("\n--Valores--");
                                           printf("\nEl valor de V = %.3f volts",v);
                                           printf("\nEl valor de R1 = %.3f ohms",r1);
                                           printf("\nEl valor de R2 = %.3f ohms",r2);
                                           printf("\nEl valor de R3 = %.3f ohms",r3);
                                           printf("\nEl valor de R4 = %.3f ohms",r4);
                                           printf("\nEl valor de R5 = %.3f ohms",r5);
                                           printf("\nEl valor de R6 = %.3f ohms",r6);
                                           printf("\nEl valor de R7 = %.3f ohms",r7);
                                           printf("\nEl valor de R8 = %.3f ohms",r8);
                                           printf("\nEl valor de R9 = %.3f ohms",r9);
                                           printf("\nEl valor de R10 = %.3f ohms",r10);
                                           printf("\nEl valor de R = %.3f ohms",r);
                                                printf("\n\n__Simbologia__");
                                                printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                                for (int ren=1; ren<=Resistencias; ren++) {//renglones
                                                    
                                                    for (int col=1; col<=Resistencias; col++) {//columnas
                                                       
                                                        if (ren == 1||ren==Resistencias) {
                                                            printf("--");}
                                                        if (col==1 && ren>1 && ren<Resistencias-1) {
                                                            printf("|\n*");}
                                                        if (ren==Resistencias-1 && col==1) {
                                                            printf("|");}
                                                        if (ren==1 && col==1) {
                                                            printf("*");}
                                                        if (ren==1 && col==Resistencias-1) {
                                                            printf("*");}
                                                        if (ren==Resistencias && col==2) {
                                                            printf("*");}
                                                        if (ren==1 && col==Resistencias) {
                                                            printf(" (+)%.2fvolts",v);}
                                                        if (ren==Resistencias && col==Resistencias) {
                                                            printf(" (-)");}
                                                        
                                                    }
                                                    printf("\n");
                                                }
                                        }
                                            break;
                                        
                                        default:
                                            if (Resistencias>10) {
                                                printf("\n**El maximo de voltajes es 10**");
                                            }
                                            break;
                                    }
                                
                                
                                
                                printf("\n\n1. Repetir\n");
                                printf("2. Regresar\n");
                                printf("Elija una opcion:");
                                scanf("%d",&menu3);
                                
                            }while(menu3!=2);
                        }break;
                            
                        case 3:{ // 3. Calcular corriente
                            int menu3;
                            do {
                                printf("\n\nIntroduzca el valor de V: "); scanf("%f", &V);
                                printf("Introduzca el valor de R: "); scanf("%f", &R);
                                   I = V/R;
                                printf("\nEl Valor de I = %.3fampers", I);
                                 printf("\n\n__Simbologia__");
                                 printf("\n\n-,|  Cable \n *  Resistencia\n(+)  Catodo\n(-)  Anodo\n\n");
                                 printf("(-) ---*--- %.2fvolts(+)",V);
                                
                                printf("\n\n1. Repetir\n");
                                printf("2. Regresar\n");
                                printf("Elija una opcion:");
                                scanf("%d",&menu3);
                                
                            } while (menu3!=2);
                        }break;
                        
                        case 4:
                            break;
                            
                        default:
                            printf("\n**Opcion invalida**");
                            break;
                    }
                } while (menu2!=4);
                
            }break;
                
           case 2:{//CIRCUITO PARALELO
                
               int r;


                       do{
                       printf("\n\n---CIRCUITO PARALELO--- \n \n \n");
                       printf("Digite el numero de resistencias que tiene el circuito \n \n");
                       printf("Digite 11 para regresar \n \n");
                           scanf("%d",&r);

                           switch (r) {

                       // SENCILLO

                                   case 1:{

                                   int a,a1,a2,a3;
                                   float voltaje,corriente,resistencia;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 1 RESISTENCIA \n");
                                   printf("\n    ------- \n");
                                   printf("    |     | \n");
                                   printf("VCC =     * \n");
                                   printf("    |     | \n");
                                   printf("    ------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&a);

                                       switch (a) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica: ");
                                               scanf("%f",&resistencia);

                                           printf("\n Introduzca el valor de la corriente: ");
                                               scanf("%f",&corriente);

                                           voltaje = corriente * resistencia;

                                           printf("\n El valor del voltaje es de %f V \n",voltaje);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&a1);

                                           } while(a1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica: ");
                                               scanf("%f",&resistencia);

                                           printf("\n Introduzca el valor del voltaje: ");
                                               scanf("%f",&voltaje);

                                           corriente = voltaje / resistencia;

                                           printf("\n El valor de la intensidad de corriente es de %f A \n",corriente);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&a2);

                                           } while(a2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor de la corriente: ");
                                               scanf("%f",&corriente);

                                           printf("\n Introduzca el valor del voltaje: ");
                                               scanf("%f",&voltaje);

                                           resistencia = voltaje / corriente;

                                           printf("\n El valor de la resistencia es de %f ohm \n",resistencia);


                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&a3);

                                           } while(a3!=2);
                                           }
                                           break;
                                       }
                                   } while(a!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 2:{

                                   int b,b1,b2,b3;
                                   float v,i,i1,i2,it,r,r1,r2,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 2 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------- \n");
                                   printf("    |     |     | \n");
                                   printf("VCC =     *     * \n");
                                   printf("    |     |     | \n");
                                   printf("    ------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&b);

                                       switch (b) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la intensidad de corriente de dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&b1);

                                           } while(b1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           it = i1 + i2;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&b2);

                                           } while(b2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           rt = 1 / ((1/r1)+(1/r2));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&b3);

                                           } while(b3!=2);
                                           }
                                           break;
                                       }
                                   } while(b!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 3:{

                                   int c,c1,c2,c3;
                                   float v,i,i1,i2,i3,it,r,r1,r2,r3,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 3 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------- \n");
                                   printf("    |     |     |     | \n");
                                   printf("VCC =     *     *     * \n");
                                   printf("    |     |     |     | \n");
                                   printf("    ------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&c);

                                       switch (c) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&c1);

                                           } while(c1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           it = i1 + i2 + i3;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&c2);

                                           } while(c2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&c3);

                                           } while(c3!=2);
                                           }
                                           break;
                                       }
                                   } while(c!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 4:{

                                   int d,d1,d2,d3;
                                   float v,i,i1,i2,i3,i4,it,r,r1,r2,r3,r4,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 4 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------- \n");
                                   printf("    |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     * \n");
                                   printf("    |     |     |     |     | \n");
                                   printf("    ------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&d);

                                       switch (d) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&d1);

                                           } while(d1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           it = i1 + i2 + i3 + i4;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&d2);

                                           } while(d2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&d3);

                                           } while(d3!=2);
                                           }
                                           break;
                                       }
                                   } while(d!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 5:{

                                   int e,e1,e2,e3;
                                   float v,i,i1,i2,i3,i4,i5,it,r,r1,r2,r3,r4,r5,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 5 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------- \n");
                                   printf("    |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     | \n");
                                   printf("    ------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&e);

                                       switch (e) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&e1);

                                           } while(e1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           i5 = v / r5;
                                           it = i1 + i2 + i3 + i4 + i5;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",i5);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&e2);

                                           } while(e2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&i5);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           r5 = v / i5;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&e3);

                                           } while(e3!=2);
                                           }
                                           break;
                                       }
                                   } while(e!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 6:{

                                   int f,f1,f2,f3;
                                   float v,i,i1,i2,i3,i4,i5,i6,it,r,r1,r2,r3,r4,r5,r6,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 6 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------------- \n");
                                   printf("    |     |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     |     | \n");
                                   printf("    ------------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&f);

                                       switch (f) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&f1);

                                           } while(f1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor de la resistencia electrica 6: \n");
                                               scanf("%f",&r6);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           i5 = v / r5;
                                           i6 = v / r6;
                                           it = i1 + i2 + i3 + i4 + i5 + i6;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",i5);
                                           printf("\n El valor de la corriente 6 es de %f A \n",i6);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&f2);

                                           } while(f2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&i5);

                                           printf("\n Introduzca el valor de la corriente 6: \n");
                                               scanf("%f",&i6);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           r5 = v / i5;
                                           r6 = v / i6;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5)+(1/r6));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia 6 es de %f Ohms \n",r6);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&f3);

                                           } while(f3!=2);
                                           }
                                           break;
                                       }
                                   } while(f!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 7:{

                                   int g,g1,g2,g3;
                                   float v,i,i1,i2,i3,i4,i5,i6,i7,it,r,r1,r2,r3,r4,r5,r6,r7,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 7 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------------------- \n");
                                   printf("    |     |     |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     |     |     |\n");
                                   printf("    ------------------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&g);

                                       switch (g) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&g1);

                                           } while(g1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor de la resistencia electrica 6: \n");
                                               scanf("%f",&r6);

                                           printf("\n Introduzca el valor de la resistencia electrica 7: \n");
                                               scanf("%f",&r7);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           i5 = v / r5;
                                           i6 = v / r6;
                                           i7 = v / r7;
                                           it = i1 + i2 + i3 + i4 + i5 + i6 + i7;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",i5);
                                           printf("\n El valor de la corriente 6 es de %f A \n",i6);
                                           printf("\n El valor de la corriente 7 es de %f A \n",i7);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&g2);

                                           } while(g2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&i5);

                                           printf("\n Introduzca el valor de la corriente 6: \n");
                                               scanf("%f",&i6);

                                           printf("\n Introduzca el valor de la corriente 7: \n");
                                               scanf("%f",&i7);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           r5 = v / i5;
                                           r6 = v / i6;
                                           r7 = v / i7;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5)+(1/r6)+(1/r7));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia 6 es de %f Ohms \n",r6);
                                           printf("\n El valor de la resistencia 7 es de %f Ohms \n",r7);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&g3);

                                           } while(g3!=2);
                                           }
                                           break;
                                       }
                                   } while(g!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 8:{

                                   int h,h1,h2,h3;
                                   float v,i,i1,i2,i3,i4,i5,i6,i7,i8,it,r,r1,r2,r3,r4,r5,r6,r7,r8,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 8 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------------------------- \n");
                                   printf("    |     |     |     |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     |     |     |     |\n");
                                   printf("    ------------------------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&h);

                                       switch (h) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&h1);

                                           } while(h1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor de la resistencia electrica 6: \n");
                                               scanf("%f",&r6);

                                           printf("\n Introduzca el valor de la resistencia electrica 7: \n");
                                               scanf("%f",&r7);

                                           printf("\n Introduzca el valor de la resistencia electrica 8: \n");
                                               scanf("%f",&r8);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           i5 = v / r5;
                                           i6 = v / r6;
                                           i7 = v / r7;
                                           i8 = v / r8;
                                           it = i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",i5);
                                           printf("\n El valor de la corriente 6 es de %f A \n",i6);
                                           printf("\n El valor de la corriente 7 es de %f A \n",i7);
                                           printf("\n El valor de la corriente 8 es de %f A \n",i8);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&h2);

                                           } while(h2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&i5);

                                           printf("\n Introduzca el valor de la corriente 6: \n");
                                               scanf("%f",&i6);

                                           printf("\n Introduzca el valor de la corriente 7: \n");
                                               scanf("%f",&i7);

                                           printf("\n Introduzca el valor de la corriente 8: \n");
                                               scanf("%f",&i8);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           r5 = v / i5;
                                           r6 = v / i6;
                                           r7 = v / i7;
                                           r8 = v / i8;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5)+(1/r6)+(1/r7)+(1/r8));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia 6 es de %f Ohms \n",r6);
                                           printf("\n El valor de la resistencia 7 es de %f Ohms \n",r7);
                                           printf("\n El valor de la resistencia 8 es de %f Ohms \n",r8);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&h3);

                                           } while(h3!=2);
                                           }
                                           break;
                                       }
                                   } while(h!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 9:{

                                   int i,i1,i2,i3;
                                   float v,c,c1,c2,c3,c4,c5,c6,c7,c8,c9,ct,r,r1,r2,r3,r4,r5,r6,r7,r8,r9,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 9 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------------------------------- \n");
                                   printf("    |     |     |     |     |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     |     |     |     |     |\n");
                                   printf("    ------------------------------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&i);

                                       switch (i) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&c);

                                           v = c * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&i1);

                                           } while(i1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor de la resistencia electrica 6: \n");
                                               scanf("%f",&r6);

                                           printf("\n Introduzca el valor de la resistencia electrica 7: \n");
                                               scanf("%f",&r7);

                                           printf("\n Introduzca el valor de la resistencia electrica 8: \n");
                                               scanf("%f",&r8);

                                           printf("\n Introduzca el valor de la resistencia electrica 9: \n");
                                               scanf("%f",&r9);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           c1 = v / r1;
                                           c2 = v / r2;
                                           c3 = v / r3;
                                           c4 = v / r4;
                                           c5 = v / r5;
                                           c6 = v / r6;
                                           c7 = v / r7;
                                           c8 = v / r8;
                                           c9 = v / r9;
                                           ct = c1 + c2 + c3 + c4 + c5 + c6 + c7 + c8 + c9;

                                           printf("\n El valor de la corriente 1 es de %f A \n",c1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",c2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",c3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",c4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",c5);
                                           printf("\n El valor de la corriente 6 es de %f A \n",c6);
                                           printf("\n El valor de la corriente 7 es de %f A \n",c7);
                                           printf("\n El valor de la corriente 8 es de %f A \n",c8);
                                           printf("\n El valor de la corriente 9 es de %f A \n",c9);
                                           printf("\n El valor de la corriente total es de %f A \n",ct);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&i2);

                                           } while(i2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&c1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&c2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&c3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&c4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&c5);

                                           printf("\n Introduzca el valor de la corriente 6: \n");
                                               scanf("%f",&c6);

                                           printf("\n Introduzca el valor de la corriente 7: \n");
                                               scanf("%f",&c7);

                                           printf("\n Introduzca el valor de la corriente 8: \n");
                                               scanf("%f",&c8);

                                           printf("\n Introduzca el valor de la corriente 9: \n");
                                               scanf("%f",&c9);

                                           r1 = v / c1;
                                           r2 = v / c2;
                                           r3 = v / c3;
                                           r4 = v / c4;
                                           r5 = v / c5;
                                           r6 = v / c6;
                                           r7 = v / c7;
                                           r8 = v / c8;
                                           r9 = v / c9;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5)+(1/r6)+(1/r7)+(1/r8)+(1/r9));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia 6 es de %f Ohms \n",r6);
                                           printf("\n El valor de la resistencia 7 es de %f Ohms \n",r7);
                                           printf("\n El valor de la resistencia 8 es de %f Ohms \n",r8);
                                           printf("\n El valor de la resistencia 9 es de %f Ohms \n",r9);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&i3);

                                           } while(i3!=2);
                                           }
                                           break;
                                       }
                                   } while(i!=4);
                                   }
                                   break;

                       // EXTRA

                                   case 10:{

                                   int j,j1,j2,j3;
                                   float v,i,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10,it,r,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,rt;

                                   do{

                                   printf("\n - DIAGRAMA DE UN CIRCUITO ELECTRICO CON 10 RESISTENCIAS EN PARALELO \n");
                                   printf("\n    ------------------------------------------------------------- \n");
                                   printf("    |     |     |     |     |     |     |     |     |     |     | \n");
                                   printf("VCC =     *     *     *     *     *     *     *     *     *     * \n");
                                   printf("    |     |     |     |     |     |     |     |     |     |     |\n");
                                   printf("    ------------------------------------------------------------- \n");

                                   printf("\n ---Seleccione la variable que desea calcular--- \n \n");
                                   printf("1. Voltaje \n \n");
                                   printf("2. Intensidad de corriente \n \n");
                                   printf("3. Resistencia electrica \n \n");
                                   printf("4. Regresar \n \n");
                                   scanf("%d",&j);

                                       switch (j) {

                                           case 1:{
                                           do{
                                           printf("\n CALCULO DEL VOLTAJE \n");

                                           printf("\n Introduzca el valor de una resistencia electrica: \n");
                                               scanf("%f",&r);

                                           printf("\n Introduzca el valor de la corriente en dicha resistencia: \n");
                                               scanf("%f",&i);

                                           v = i * r;

                                           printf("\n El valor del voltaje es de %f V \n",v);
                                           printf(" (Por ser un circuito paralelo tenemos que todos los voltajes son iguales) \n");

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&j1);

                                           } while(j1!=2);
                                           }
                                           break;

                                           case 2:{
                                           do{
                                           printf("\n CALCULO DE LA CORRIENTE \n");

                                           printf("\n Introduzca el valor de la resistencia electrica 1: \n");
                                               scanf("%f",&r1);

                                           printf("\n Introduzca el valor de la resistencia electrica 2: \n");
                                               scanf("%f",&r2);

                                           printf("\n Introduzca el valor de la resistencia electrica 3: \n");
                                               scanf("%f",&r3);

                                           printf("\n Introduzca el valor de la resistencia electrica 4: \n");
                                               scanf("%f",&r4);

                                           printf("\n Introduzca el valor de la resistencia electrica 5: \n");
                                               scanf("%f",&r5);

                                           printf("\n Introduzca el valor de la resistencia electrica 6: \n");
                                               scanf("%f",&r6);

                                           printf("\n Introduzca el valor de la resistencia electrica 7: \n");
                                               scanf("%f",&r7);

                                           printf("\n Introduzca el valor de la resistencia electrica 8: \n");
                                               scanf("%f",&r8);

                                           printf("\n Introduzca el valor de la resistencia electrica 9: \n");
                                               scanf("%f",&r9);

                                           printf("\n Introduzca el valor de la resistencia electrica 10: \n");
                                               scanf("%f",&r10);

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           i1 = v / r1;
                                           i2 = v / r2;
                                           i3 = v / r3;
                                           i4 = v / r4;
                                           i5 = v / r5;
                                           i6 = v / r6;
                                           i7 = v / r7;
                                           i8 = v / r8;
                                           i9 = v / r9;
                                           i10 = v / r10;
                                           it = i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8 + i9 + i10;

                                           printf("\n El valor de la corriente 1 es de %f A \n",i1);
                                           printf("\n El valor de la corriente 2 es de %f A \n",i2);
                                           printf("\n El valor de la corriente 3 es de %f A \n",i3);
                                           printf("\n El valor de la corriente 4 es de %f A \n",i4);
                                           printf("\n El valor de la corriente 5 es de %f A \n",i5);
                                           printf("\n El valor de la corriente 6 es de %f A \n",i6);
                                           printf("\n El valor de la corriente 7 es de %f A \n",i7);
                                           printf("\n El valor de la corriente 8 es de %f A \n",i8);
                                           printf("\n El valor de la corriente 9 es de %f A \n",i9);
                                           printf("\n El valor de la corriente 10 es de %f A \n",i10);
                                           printf("\n El valor de la corriente total es de %f A \n",it);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&j2);

                                           } while(j2!=2);
                                           }
                                           break;

                                           case 3:{
                                           do{
                                           printf("\n CALCULO DE LA RESISTENCIA \n");

                                           printf("\n Introduzca el valor del voltaje: \n");
                                               scanf("%f",&v);

                                           printf("\n Introduzca el valor de la corriente 1: \n");
                                               scanf("%f",&i1);

                                           printf("\n Introduzca el valor de la corriente 2: \n");
                                               scanf("%f",&i2);

                                           printf("\n Introduzca el valor de la corriente 3: \n");
                                               scanf("%f",&i3);

                                           printf("\n Introduzca el valor de la corriente 4: \n");
                                               scanf("%f",&i4);

                                           printf("\n Introduzca el valor de la corriente 5: \n");
                                               scanf("%f",&i5);

                                           printf("\n Introduzca el valor de la corriente 6: \n");
                                               scanf("%f",&i6);

                                           printf("\n Introduzca el valor de la corriente 7: \n");
                                               scanf("%f",&i7);

                                           printf("\n Introduzca el valor de la corriente 8: \n");
                                               scanf("%f",&i8);

                                           printf("\n Introduzca el valor de la corriente 9: \n");
                                               scanf("%f",&i9);

                                           printf("\n Introduzca el valor de la corriente 10: \n");
                                               scanf("%f",&i10);

                                           r1 = v / i1;
                                           r2 = v / i2;
                                           r3 = v / i3;
                                           r4 = v / i4;
                                           r5 = v / i5;
                                           r6 = v / i6;
                                           r7 = v / i7;
                                           r8 = v / i8;
                                           r9 = v / i9;
                                           r10 = v / i10;
                                           rt = 1 / ((1/r1)+(1/r2)+(1/r3)+(1/r4)+(1/r5)+(1/r6)+(1/r7)+(1/r8)+(1/r9)+(1/r10));

                                           printf("\n El valor de la resistencia 1 es de %f Ohms \n",r1);
                                           printf("\n El valor de la resistencia 2 es de %f Ohms \n",r2);
                                           printf("\n El valor de la resistencia 3 es de %f Ohms \n",r3);
                                           printf("\n El valor de la resistencia 4 es de %f Ohms \n",r4);
                                           printf("\n El valor de la resistencia 5 es de %f Ohms \n",r5);
                                           printf("\n El valor de la resistencia 6 es de %f Ohms \n",r6);
                                           printf("\n El valor de la resistencia 7 es de %f Ohms \n",r7);
                                           printf("\n El valor de la resistencia 8 es de %f Ohms \n",r8);
                                           printf("\n El valor de la resistencia 9 es de %f Ohms \n",r9);
                                           printf("\n El valor de la resistencia total es de %f Ohms \n",rt);

                                           printf("\n 1. Repetir \n");
                                           printf("\n 2. Regresar \n");
                                               scanf("%d",&j3);

                                           } while(j3!=2);
                                           }
                                           break;
                                       }
                                   } while(j!=4);
                                   }
                                   break;
                                   }


                       } while(r!=11);

               
            }break;
                
            case 3:
                
             break;
                
            default:
             printf("Opcion invalida");
             break;
        }
    } while (menu1!=3);
    
    return 0;
}

